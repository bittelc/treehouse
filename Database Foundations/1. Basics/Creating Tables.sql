-- Basic SQL statement to create a table
-- Syntax: create (table/row/...) name (columnName dataType(sizeOfDataType))
-- Create a table named actors with a single column with the label "name". Data in name can be 50 characters long.
create table actors (name varchar(50));

-- Multiple columns
-- Creates a table named movies with two columngs. The title column can store varchars with up to 200 characters and the year column can only store integers.
create table movies (title varchar(200), year integer);
