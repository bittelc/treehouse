/* Columns are defined in the schema to store data in a particular type: String, numeric, and date and time types.
For string data types -- Contain a sequence of characters. Two main types of string data: varchar and text. Varchar is used primarily for short things like movie titles or nouns. Text is usually used for longer things like descriptions of movies. Both have limitations.
Numeric types: integer (whole numbers), fixed point (decimal. Fixed precision. Dollars are good for this.), floating point (non-exact way. Not fixed to particular precision).
Date & time types: Store temporal data (data related to time and date). Datetime is when both data and time are stored.
Storing data in numeric type usually requires smaller storage on disc than string */