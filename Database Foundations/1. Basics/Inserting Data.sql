-- A row contains a single element of data within the table.
-- Syntax:
-- insert into tableName values (data1, data2, data3)
-- Ex:
insert into movies values ("Avatar", 2009);
-- Order of data is important. Must be same as order of labels of columns.