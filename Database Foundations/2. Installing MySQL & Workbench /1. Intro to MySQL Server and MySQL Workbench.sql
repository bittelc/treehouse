-- Intro to MySQL Server and MySQL Workbench
-- Client-server model:
-- 1. Client sends a SQL statement to the server. 
-- 2. Server then processes the statement and will return a dataset depending on the statement.
-- 3. The client will then display the dataset
-- MySQL Workbench is the client. MySQL Community Server will serve as the server.
