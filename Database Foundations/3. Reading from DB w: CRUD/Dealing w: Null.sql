-- To evaluate null values, cannot use the = to do so. The below line returns no rows in the result set
select * from movies where year = null; 

-- Must use the "is" statement.
-- Find all rows in DB with no value in years
select * from movies where year is null;

-- Can filter out null rows w/ where clause and IS NOT
SELECT * FROM movies WHERE year IS NOT null ORDER BY year;