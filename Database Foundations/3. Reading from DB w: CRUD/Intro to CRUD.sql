Intro to CRUD

-- Create, Retrieve, Update and Delete (CRUD) refers to the four major functions implemented in DB applications.
-- We'll take a look more in-depth at the READ method which, in SQL, uses the method "select".