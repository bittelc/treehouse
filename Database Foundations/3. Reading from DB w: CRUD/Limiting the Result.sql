/* Use the "limit" keyword followed by the number of elements that you want to limit the result set to. Default limit is set to 1000. */
Select * from movies limit 10;
-- Above statement brings back first 10 rows in movies table

-- Can offset this result set with offset keyword. This skips the first 3 rows in the returned result set.
Select * from movies limit 10 offset 3;

-- Could alternatively write the above line with:
select * from movies limit 3, 10;