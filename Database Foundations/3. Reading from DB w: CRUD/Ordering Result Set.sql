/* The default result set sorting mechanism is to display the way in which they are entered into the table. In "treehouse_movie_db" they were entered alphabetically by movie title. We can sort by year instead. */
-- Default result for ordering by column with values as integers is to order by ascending value
select * from movies order by year;

-- To order by descending integer
select * from movies order by year desc;

-- Can chain ordering methods together
select * from movies order by year desc, title asc;