/* A clause allows you to filter a result set, instead of selecting all of the information within it. Using the "where" clause like an "if" statement. if clause is true, include in result set. if clause is not true, filter from result set. */

-- In many languages, an "==" sign applies to test if two conditions are equal or even "===". Not case in SQL.
select * from movies where year = 1999;
-- All rows (data) within movies in which the year column has a value of 1999 are returned.

-- "!=" means not equal to
select * from movies where year != 1999;

select * from movies where year > 1999;
select * from movies where year >= 1999;

-- Can also combine clauses together.
select * from movies where year = 1999 and title = "The Matrix";

-- Can search for a string within DB too. To do this, use a combination of the keyword "like" and wildcards. "like" is a clause that filters results independent of capitalization. Wild cards allow for the allowance characters before, after, or in between search criteria.
select * from movies where title like "%godfather";
/* Above returns "The Godfather". G=g because of like. The =% because it's a wildcard. */
select * from movies where title like "%godfather%";
-- Above returns both "The Godfather" and "The Godfather Part II"