/* To select a DB s.t. one can run queries against it simply open up the MySQLWorkbench script editor. On the left pane, double click on the DB for which to query from.
*/

-- "Select" tells DB to READ data. "from" is keyword to tell DB where to get data from. In this case, it's the "movies" table. The "*" means all columns.
Select * from movies;
/* Above line could be replaced with:
Select movies.title, movies.year from movies;
Above line would allow us to alter the order of the columns by switching around the items when calling it.
Could also do:
Select title, year from movies;
*/