-- A DB  is a container that stores a table and data for an app or part of an app.
-- To create a DB go to menu bar above this code and click on the icon that looks like a cylinder cut into 3 pieces. Not the nav bar that is attached to this code, but the nav bar that is at the uppermost section. By clicking this icon, we are using a GUI to create the DB, rather than doing it by coding. The code for creating a DB named "This_DB" would be:
-- create schema if not exists "This_DB";
-- The term "schema" and "database" can be used interchangeably.
-- Rather than double clicking to select a DB, can use the keyword "use"
use Coles_DB_Treehouse;
-- Can now run queries against the DB.

