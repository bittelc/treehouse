use Coles_DB_Treehouse;
/* to create a table within a DB, simply: */
create table if not exists friends (name varchar(20));
/* The above line creates a table named "friends" with a single column in that table with label of "name". The data within "name" column can store variables as characters up to 20 characters in length. */

create table if not exists numbers (spelled varchar(20) null, nums integer not null); -- The "not null" statement means that there can be no blank rows or entries in the column, For every entry in the "spelled" column, there must be a corresponding entry in the "nums" column. However, if ther eis an entry in the "nums" column, there does not have to be an entry in the "spelled" column. "null" is the default value.