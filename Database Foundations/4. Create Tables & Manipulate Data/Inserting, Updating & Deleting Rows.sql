/* CRUD acronym in mySQL commands:
Create: "insert"
Read: "select"
Update: "update"
Delete: "remove"
*/
use treehouse_movie_db;

-- to insert data into the database
/* syntax:
insert into tableName values ("dataNameForColumn1", "dataforColumn2", integerForColumn3 ...);
*/
insert into movies values ("Avatar", 2009);
/* Could also have written as same insertion:
insert into movies (title, year) values ("Avatar", 2009); or
insert into movies (year, title) values (2009, "Avatar"); */

insert into movies values ("Avatar 2", 2014), ("Avatar 3", null); -- inserts both Avatar 2 and Avatar 3 into movies table

insert into movies SET title="Back to the Future", year=1985;

-- to update data in DB
update movies set year=2015 where title="Avatar 3"; -- Sets year of Avatar 3 to 2015. Previously, was null. Upon running this script, there's an error: "You are using safe update mode". To dodge out of safe update mode:
set sql_safe_updates = 0; -- turn off safe update mode
-- update multiple values: Avatar 3 = Avatar Reloaded
update movies set year = 2016, title="Avatar Reloaded" where title="Avatar 3";

-- Delete = "delete"
delete from movies where title="Avatar 2" and year=2016; -- year clause added just to be safe

-- It's a good idea to set safe updates mode on after you're done.
set sql_safe_updates=1;