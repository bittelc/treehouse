use Coles_DB_Treehouse;

/* Syntax for renaming table:
rename whatToRename to newNew
*/
rename table friends to friendsTable, numbers to numbersTable;
-- both tables were renamed

-- deleting table from entire schema:
drop table if exists deletedTable;

-- remove all data from within table:
truncate table deletedTable;

