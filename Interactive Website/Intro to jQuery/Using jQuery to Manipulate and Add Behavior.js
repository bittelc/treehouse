 // To hide an element, method is "hide()" which takes no arguments. Syntax is:
 $("#myElement").hide(); // where "#myElement" is any capable element for selecting.

 // To add elements in before other elements, use "before(arg)" method.
 $("#myElement").before("<h1>Hello</h1>");
 // Inserts an 1st level header of "Hello" before the selected #myElement element.


// "click" method will take functions as arguments. These functions are called "handler"s. 
 $("#myElement").click(
 	function (){
 		// code for what happens upon clicking "myElement" here.
 	}
 	); 

 // Methods can be chained together to save lines of code if applying to the same element.
 $("#myElement").hide().before("Put whatever you want to go before the hidden element here!");