// jQuery uses following syntax:
$("string representing CSS selector");

// represents jQuery to select element with id of "container"
$("#container");

//select all link elements within a nav element
$("nav a");