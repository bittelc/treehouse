/* Creating Arrays
=============================== */

// Square brackets define an array. Elements do not need to be same type, like all strings.
var x = ["some", 3, "words"];

console.log(x.length); // This displays how many elements are within the array. Answer: 3

var y = ["a string", 3, true, function(a,b){return a + b;}]
// There are 4 elements in the above array

// Can add arrays within arrays
var z = [1,2,[x],3]; // Array has 4 elements

// Can create empty arrays;
var a = [];

// ===================================================
// Array constructor is not recommended. More difficult to create and use. Below:

// This method, however, is good for initializing the size of an array before the elements are known.
// This array has 4 elements but nothin in the elements.
var a = new Array(4);