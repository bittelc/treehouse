
/* Retrieving Values
=============================== */
/* Index of array always starts from 0. Syntax to retrive array:
var variableName = array_to_access[a] // where "a" is the element position
*/

var array = ["some", 3, "words"];
var first = array[0]; //In this case, first = "some"

// This array has 3 elements, all of which are functions.
var funcArray = [function (a) {return a *2;}, function (a) {return a*3;}, function (a,b) {return a + b;}]; 

// Now let's pass arguments to that array
var retriever = funcArray[0]; // retriever is set to the 1st function in funcArray
var output = retriever(3); // output is the calculation of retriever with a passed input of 3

/* Assigning Values
=============================== */
// This sets element 2 within array (which was previously the middle function), to the value 42
funcArray[1] = 42;

// Add to previous array
var myArray = [1,2,3]; // Array defined with 3 elements and values assigned for those elements
myArray[3] = 4; // Array resized with additional element. Now looks like myArray = [1,2,3,4];


myArray[7] = 8;  // So now we have resized the array but not input elements in between the last addition and the previous size of the array. JS fills these gaps with undefined values.
/* myArray currently looks like:
myArray = [1,2,3,4, undefined, undefined, undefined, 8];
But check out Chrome's syntax for this. Copy and pasted:
myArray = [1, 2, 3, 4, undefined × 3, 8]
*/

// To add specifically to the end of the array, use .length method to attack that spot
myArray[myArray.length] = myArray.length+1 // Now myArray looks like:
// myArray = [1,2,3,4, undefined, undefined, undefined, 8, 9]