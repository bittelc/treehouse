
/* Methods Pt. I

===================================== */

// A few quirky things happen in JS. For instance console.log() is actually run at the end of the code.


/* the ".push(a)" method adds an element with value, a, to the end of the array. the ".toString" method is used because console.log alone actually runs this command at the end of the code. */
var my_array = [2, 3, 4];
console.log(my_array.toString());
// onsole.log(my_array) Answer: 2,3,4,5 because runs at end of code rather than sequentially through lines.

my_array.push(5);
console.log(my_array.toString()); // Answer: 2,3,4,5


// ".pop()" method evaluates, and removes the last element in the array. ".pop()" does not use any inputs.
var last = my_array.pop(); // last = 5
console.log(my_array.toString()); //Answer: 2,3,4

// ".unshift()" is analagous to .push() method, except instead of adding element to end of array, we'll add it to the beginning
my_array.unshift(1);
console.log(my_array.toString()); // Answer: 1,2,3,4


// ".shift" is analogous to .pop() method except instead of removing and evaluating last element, does it to first element
var first = my_array.shift(); // first = 1
console.log(my_array.toString()); //Answer: 2,3,4