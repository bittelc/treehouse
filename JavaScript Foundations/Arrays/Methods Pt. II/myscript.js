
/* Methods Pt. II
===================================== */


var my_array = [4,6,0,-44,-3,1.4,4,42,65];
console.log(my_array.toString());


// Sort doesn't actually work all that well. ".sort()" sorts items as if they were strings, so not all that effective for an array of numbers.
my_array.sort();
console.log(my_array.toString());
// Answer: -3, -44, 0, 1.4, 4, 4, 42, 6, 65

// Can change how sort works by providing comparative function.
my_array.sort(function (a,b) 
		{return (a-b)
		}
)
console.log(my_array.toString());

// Can sort by random order with 
my_array.sort(function ()
	{
		return (Math.random() - .5);
	}
)
console.log(my_array.toString());

// Can sort in reverse too
my_array.reverse();
console.log(my_array.toString());


// Quiz: 1) Sort saying1 in reverse order. 2) Sort saying2 by length of the word. Shortest first

 var saying1 = ["The", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"];
 var saying2 = ["The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog's", "back"];
 saying1 = saying1.reverse();
 saying2 = saying2.sort( function(a,b)
 	{return (a.length - b.length)
 	}
 )
 console.log(saying2.toString());