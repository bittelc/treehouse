
/* Splice
===================================== */

var a = [1,2,3,4,5,6,7,8,9];
console.log(a.toString());

// Splice allows for removing of certain elements within an array
delete a[1];
