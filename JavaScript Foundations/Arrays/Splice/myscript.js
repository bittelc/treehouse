
/* Splice
===================================== */

var a = [1,2,3,4,5,6,7,8,9];
console.log(a.toString());

// the "delete" function does not work exactly as one might think
delete a[2]; // Hopefully will delete element 3 (3) of a.
console.log(a.toString()); //Answer: [1,2, undefined, 4,5,6,7,8,9].
// Rather than remove the element and close the gap, "delete" just removes the element.



// Splice allows us to remove element AND close gap left by removing element
// ".splice(a,b,c)" for splicing. a represents element index to operate on. b represents how many elements after index do you want to remove. c (optional) represents value to be inserted at a. c can be any number of elements separated by commas. So it's more like (a,b,c,d,e...);
a.splice(2,1); // a = 2 b = 1
console.log(a.toString()); //Answer: [1, 2, 4, 5, 6, ,7 ,8, 9]

var a = [1,2,3,4,5,6,7,8,9];
// Revalue array
a.splice(2,3);
console.log(a.toString()); // Answer: [1, 2, 6, 7, 8, 9]


var a = [1,2,3,4,5,6,7,8,9];
//Can also choose to insert indexes and elements rather than remove them by making b=0. 
a.splice(5, 0, "five point five"); // Working with sixth element, removing no elements, and inserting string value of "five point five"
console.log(a.toString()); // Answer: [1,2,3,4,five point five,5,6,7,8,9 ]
