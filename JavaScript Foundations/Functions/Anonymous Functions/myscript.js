/* Anonymous Functions
============================== */

// Anonymous functions don't have names, but the variables can have names
// Anonymous functions are just variables with coding within it.
var my_function = function () {
	console.log("my function was called");
//	undeclared_variable; // will result in a reference variable. But because this function is defined in that it was assigned to a variable, when debugging with Chrome, it can see exactly where the fuckup arises.
}

var call_twice = function(another_function) {
	another_function(); // call_twice can only take in arguments that are functions as a result of the parenthesis
	another_function();
};

call_twice(function () {
	//evaluate at function output, and then pass in as "another_function"
	console.log("Hello from the anonymous function");
});

// Can create function and immediately call the function
(function  (a,b) {
	//... Code
	console.log("from anon function " + a +b);
})("arg1", " arg2") // Parenthesis around are for syntax reasons. Parenthesis after function mean that it is executed immediately.
// Answer: "from anon function arg1 arg2"
