/* Arguments
============================== */

/* Basic syntax for function:
function func_name(arg1,arg2,arg3) {
	//function code below
	return {
	
	}
}
"say_hello" is the function name
"a" & "b" are input functions
*/


function say_hello (a,b) {
	console.log(a+b);
}

/* to call a function in the console, type function's name and parenthesis following. Within parenthesis should be the input arguments
When executing code, an extra "undefined" line appears below the run code */

say_hello(1,3); // logged to screen: "4" \n "undefined"





function namer (arg1, arg2){
	if (typeof arg2 === "undefined") {
		console.log("Pass another fuckin argument!");
	}
	else {
		console.log(arg1.concat(arg2));
	}
}
namer("Cole ","Bittel"); //console output: "Cole Bittel"
namer("Cole"); // console output: "pass another fuckin argument!"