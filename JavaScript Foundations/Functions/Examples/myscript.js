/* Examples
============================= */
// Document Object Model is interface with elements on page and allows one to change background color and stuff

// jQuery library is JS code written for browsers. Learn jQuery.

// "getElementbyID" is pretty standard throughout browsers
var button = document.getElementById("action"); /* button equals null if <script> tag in .html page is above button declaration. If after the action id tag, button = 
<button id="action">Action</button>
*/
var input = document.getElementById("text_field");


// function displays "clicked" in console once the user clicks the button
button.addEventListener('click', function () {
	console.log("other click event");
	input.setAttribute('value', 'Hello World'); //input "Hello World" into the text_field.
});

button.onclick = function () {
	console.log ('clicked');
}



