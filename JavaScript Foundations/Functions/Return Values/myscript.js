/* Return Values
============================== */


// Return values are outputs from functions
function demo(arg1, arg2) {
	var c_local = arg1 + arg2;
	return c_local; // The output of the function is c_local. In this example, c_local is output to the variable declaration C_Global

	// Any command after we run a "return command" doesn't run at all. Comes in handy when putting return in conditional statements
}

C_Global = demo(1,3);
console.log(C_Global);



function arrayCounter (arg1) {
  if (arg1 === undefined  || typeof arg1 === "string" || typeof arg1 === "number") {
    return 0;
  }
  else if (arg1.length > 0) {
    return arg1.length;
  }
}

var argum = "stri";
output = arrayCounter(argum);
console.log(output);

h = typeof undefined;