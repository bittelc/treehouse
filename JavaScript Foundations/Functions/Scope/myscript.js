/* Scope
============================== */

var color = "black";
var number = 2;

function show_color() {
	var color = "green"; // Shadow of original variable from within local function scope.
	number = 3; // No shadow placed. Instead, variable revalued.

	console.log(color + " within function");
	console.log(number + " within function");
}

show_color();
console.log(color + " in global"); // Shadow has been repealed because of exit from function. Now in global scope Answer: black
console.log(number + " in global");