/* Comparisons	
=============================== */

console.log(1 < 2);
console.log(1 < 1.0); //Answer: false

console.log(1 == "1"); // Answer: true
console.log(1 == "one"); // Answer: false

// "!" is called a "bang" operator
console.log(1 !== 2); // Answer: true

if (1 < 2) {
	console.log("1 is less than 2");
}
else {
	console.log("No 1 is not less than 2");
}

console.log(1 <= 2); // Answer: true