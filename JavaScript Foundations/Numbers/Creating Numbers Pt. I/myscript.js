/* Numbers Pt. I
=============================== */

// In JS, there's only 1 type of number. There's no int or dec type of number.
var a = 11,
	b = -123,
	c = -1.34;

var e = 0.1;
var f = 0.2;

// There's a rounding error. The calculation is pretty close, but not always exact. To avoid rounding errors, use whole numbers if possible.
var result = e * f;

var game = function() {
	preload: preload()
}

function preload() {
	var ya = prompt("Did this shit work?");
	return;
}

game.preload;
/* Numbers Pt. II
=============================== */

// To type in g = 1,000,000
var g = 1E6;

// Because this begins with number 0, JS interprets this as an 'octal' numbering system (like a hexadecimal, but with base 8 (or 0-7)). Thus h acutally equals 10. To resolve issue, never begin who numbers with a 0, especially with whole numbers that only use digits that only use 0-7 numbers.
var h=012;
// This will result in 19, because octal doesn't use a "9"
var i = 019;

// hexadecimal. This equals 16.
var i = 0x10

