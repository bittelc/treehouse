/* Math Object	
=============================== */

// The "Math" object gives a JS library for common mathematical operations

// ".random" is a function within the JS library. It generates a random number between 0 and 1.
var u = Math.random();
console.log(u);

// ".round" rounds a number up if 5 or above or down if below 5.
var v = Math.round(u * 10);
console.log(v);

// ".floor" rounds a number down
console.log(Math.floor(u * 10));

// ".ceil" rounds a number up
console.log(Math.ceil(u * 10));

// ".pow(a, b)" does an exponent. Takes a to the b power
a = Math.pow(3, 2); // Answer = 3^2 = 9;

// ."sqrt" takes sqrt of number
b = Math.sqrt(9); //Answer = 3;