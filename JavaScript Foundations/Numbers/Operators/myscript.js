/* Operators
=============================== */

var a = 7.3 + 3.1,
	b = a - 1;

// More problematic with multiplication and division, is the situation of minute scale miscalculations. However, unlike some other languages, JS doesn't "round" integers after calculating their solution. For instance 15 / 10 = 2 in some other languages because of rounding. Whereas in JS 15/10 = 1.5
var c = 1.1 * -3.2,
	d = 4/3.3;

// The "%" sign is called the "modulo" operator. It calculates the remainder from the largest divsion possible. For instance: 15 % 10 = 5. But also, 25 % 10 = 5.
var s = 15 % 10; // Answer = 5;
var e = 25 % 10; // Answer = 5;


// Order of operations is still applied in JS. 
var f = 1 + 2 * 3 / 4; // Answer = 2.5
var g = (1 + 2) * 3/4; // Answer = 2.25

/* Comparisons
=============================== */

