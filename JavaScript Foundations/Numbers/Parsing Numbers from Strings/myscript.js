/* Numbers Pt. I
=============================== */

// In JS, there's only 1 type of number. There's no int or dec type of number.
var a = 11,
	b = -123,
	c = -1.34;

var e = 0.1;
var f = 0.2;

// There's a rounding error. The calculation is pretty close, but not always exact. To avoid rounding errors, use whole numbers if possible.
var result = e * f;


/* Numbers Pt. II
=============================== */

// To type in g = 1,000,000
var g = 1E6;

// Because this begins with number 0, JS interprets this as an 'octal' numbering system (like a hexadecimal, but with base 8 (or 0-7)). Thus h acutally equals 10. To resolve issue, never begin who numbers with a 0, especially with whole numbers that only use digits that only use 0-7 numbers.
var h=012;
// This will result in 19, because octal doesn't use a "9"
var i = 019;

// hexadecimal. This equals 16.
var i = 0x10

/* Parsing Numbers from Strings
=============================== */

// Can use parseInt() to parse a whole number (integer). Now j is a number value, rather than a string. second argument is the base numbering system of the string/number preceding the comma. Below, this means 197 is a base 10 number rather than hexa or octal.
var j = parseInt("197",10);
// k = 9
var k = parseInt("01001", 2);

// Also works with larger strings
var str = parseInt("23 people",10);
// But not with, will turn up with "NaN" or not a number. Must start with a number.
var l = parseInt("there are 23 people", 10);

// to determine if a variable is equal to "NaN" use isNaN
console.log(isNaN(l));

// parseFloat() works the same way, but with floating numbers rather than strictly with integers.
n = parseFloat("123.456",10);
