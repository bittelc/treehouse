/* The "this" Keyword
Our setAge method works great for bob because it updates bob.age, but what if we want to use it for other people?

It turns out we can make a method work for many objects using a new keyword, this. The keyword this acts as a placeholder, and will refer to whichever object called that method when the method is actually used.

Let's look at the method setAge (line 2) to see how this works. By using the keyword this, setAge will change the age property of any object that calls it. Previously, we had a specific object bob instead of the keyword this. But that limited the use of the method to just bob.

Then when we say bob.setAge = setAge; (line 9), it means whenever we type bob.setAge( ), this.age in the setAge method will refer to bob.age.

Instructions
To show this way of making setAge works just like the one in exercise 2, use bob's setAge method to change his age to 50. */



// here we define our method using "this", before we even introduce bob. Cole's comment: these are methods! (functions within an object) not functions or objects! Imagine these next two lines are within an object, just thtat the object hasn't actually been defined yet.
var setAge = function (newAge) {
  this.age = newAge;
};

// now we make bob. Now we make the object for which to put the method within
var bob = new Object();
bob.age = 30;

// and down here we just use the method we already made. (combine the two!) I didn't understand why this was needed at first. But must first relate the method back to bob. Here's the error if without this next line of code:
// TypeError: Object #<Object> has no method 'setAge'
bob.setAge = setAge;
  
// change bob's age to 50 here. (Assignment to Cole from Codecademy)
bob.setAge(50);


// Now to demonstrate utility of .this , use same method with a different object! 
// make susan here, and first give her an age of 25
var susan = new Object();
susan.age = 25;
susan.setAge = setAge;

// here, update Susan's age to 35 using the method
susan.setAge(35);