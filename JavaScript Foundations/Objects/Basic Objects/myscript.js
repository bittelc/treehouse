/* JavaScript Objects
=============================== */

/* Objects store values through key described by strings
 Curly braces define object. Keys must be specified next to their value with colon. If using a key as a valid variable name, doesn't need to have parenthesis. Syntax:
 var object_name = {"key1": "value_of_key1", "key2": "value_of_key2"...};
 */

var cole = {
	favorite_girl: "Anne", // If key is a valid variable name, don't need parenthesis around it. 
	places: ["Sydney", "Lawrence", "Yellowstone", "Barrier Reef"], //If multiple values for a key, need brackets.
	friends: ["John", "Jake", "Jake", "Joe", "Collin"],
	"started dating": "February 19th" //If key not valid variabl name (space in between) need quotations

};
console.log(cole.favorite_girl);
// Answer: Anne
cole.favorite_girl = "Anne Mendelsohn";
console.log(cole.favorite_girl);
// Answer: Anne Mendelsohn

// To access alternate keys:
console.log(cole["started dating"]);

