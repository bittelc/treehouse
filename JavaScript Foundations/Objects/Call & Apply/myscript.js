/* Protypes: Part I
=============================== */



var personPrototype = { // Object created
	name: "anonymous",
	greet: function (name, mood) {
		name = name || "you";
		mood = mood || "good";
		console.log("Hello, " + name + " I am " + this.name + " and am in a " + mood + " mood!");
	},
	species: "Homo Sapien"
}

// Now we need to create a replica of personPrototype and replace the values 

function Person (name) { // capital P indicates to people that this is a special constructor function
	// this == window w/o "new"
	this.name = name; // only works because of "new"
	return this;
} 

Person.prototype = personPrototype;

me = new Person("Cole"); // "new"
you = new Person("John");