/* Custom Constructors
But this approach of adding in properties one at a time for every object is tedious! Instead of always using the boring Object constructor, we can make our own constructors.

This way we can set the properties for an object right when it is created. So instead of using the Object constructor which is empty and has no properties, we can make our own constructors which have properties.

To see how this works, look at our Person constructor in lines 1–4. This constructor is used to make Person objects. Notice it uses the keyword this to define the name and age properties and set them equal to the parameters given.

Now we can use this constructor to make our good friends bob and susan in only one line each! Look at lines 7–8: once we have the constructor, it's way easier to make people because we can include their name and age as arguments to their respective constructors.

Instructions
Practice using the constructor to make a new Person called george, whose full name is "George Washington" and age is 275. */
function Person(name,age) {
  this.name = name;
  this.age = age;
}

// Let's make bob and susan again, using our constructor
var bob = new Person("Bob Smith", 30);
var susan = new Person("Susan Jordan", 25);



/* In a constructor, we don't have to define all the properties using parameters. Look at our new Person example on line 1, and see how we set the species to be "Homo Sapiens" (line 4). This means that when we create any Person, their species will be "Homo Sapiens". In this way, the values associated with name and age are not yet assigned, but species will always have the same value.

In this case, both sally and holden will have the same species of "Homo Sapiens", which makes sense because that is the same across all people.

Instructions
Create a new object called sally using the Person constructor. Her name is "Sally Bowles" and she is 39. Create another object called holden. His name is "Holden Caulfield" and he is 16.

Edit the sentence printed out such that it includes the age of sally and holden respectively. */
function Person(name,age) {
  this.name = name;
  this.age = age;
  this.species = "Homo Sapiens";
}

var sally = new Person("Sally Bowles", 39);
var holden = new Person("Holden Caulfield", 16);
console.log("sally's species is " + sally.species + " and she is " + sally.age);
console.log("holden's species is " + holden.species + " and he is " + holden.age);
