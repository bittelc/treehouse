/* JavaScript Objects
=============================== */

var cole = {
	name: "Oh shit what's my name?",
	greet: function (this) {
		console.log("Hello, I am " + this.name);
	}// functions can also be used in values. In this case, the object is cole, the key is greet, and the value is the anonymous function!
}

cole.greet(); // calls key from object, which is a function, thus needs ().

// Method: A function that is a property of an object
// If just had a function called greet, than it would be a function, but because it is a property of an object, greet is a method.
cole.name = "Cole";
cole.greet("Cole");