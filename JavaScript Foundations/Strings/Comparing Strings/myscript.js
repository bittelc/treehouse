/* Methods for Strings 
=============================== */
var hello = "Hello"
var world = "World"
var helwo = hello + " " + world;

// ".length" is a suffix to determine the character length in the string. ".length" is called a method.
length = helwo.length;
console.log(helwo, ": ", length)

// To determine if a string of a specific character set is within the entire string, use ".indexOf()" method. Within the parenthesis should be the string that you'd like to test to see if its value is contained. 
var index = helwo.indexOf("worLD");
// Value of "index" variable is the beginning of where string sequence begins to match tested indexOf value.
console.log(index);
// Returns a -1 value  if tested not true
var index2 = helwo.indexOf("orLD");
console.log(index2);
if (helwo.indexOf("orLD") !== -1) {
	console.log("string of \"orLD\" exists within string at point: ", index2)
} else {
	console.log("string of \"orLD\" does not exist within string");
}

//"charAt()" method returns single character at element in string. 
console.log(helwo.charAt(2));




//This is how the console is cleared in Chrome!
console.clear();

/* Part II
=============================== */

//substr(start,length) takes two parameters. start index is the first character to start capturing. Length is the length of the string to capture.
var word=helwo.substr(6, 5); // The output here is word = "World"
console.log(word);

//"toLowerCase" converts the variable to lowercase letters. Because toLowerCase is a method, there must be parenthesis after it. Can apply to multiple variables with separation by comma. Same rules as "toUpperCase" method.
console.log("THISISTUESDAY",word.toLowerCase())


/* Comparing Strings
=============================== */
console.clear();
var string1= "Hello";
var string2="hello";

// "===" means (is this strictly equal to this?) whereas "==" means (if I make these two types the same, are they equal). For instance, if I ask: Is a string containing 13 characters == 13?  --> The answer may return true. Not so, if using the "===".
if (string1 === string2) {
	console.log("These strings are equal");
}
else {
	console.log("These strings are different");
}

/*
if (string1.toLowerCase === string2.toLowerCase) {
	console.log("These two have the same characters, but perhaps not the same casing");
} */

function compare(a,b) {
	//First it logs the statement to be evaluated. Then it evaluates whether or not a < b.
	console.log(a + " < " + b, a < b);
}

//Uppercase letters are always going to be less than lowercase letters. Values of letters are roughly equal to the values in the ASCII table.
compare('a', 'A');
//Compare using first character in strings. If same, move on to second character. If scame, move on to third character. Doesn't usually matter how long the word is to evaluate less than or greater than of strings. 
compare('apples', 'oranges');
// null value < 0 or any other value
compare ('app', 'apples');