/* JavaScript Strings 
=============================== */

//Simple string output = "Jim"
var name = "Jim";
console.log(name);

//Usually equivalent. 
var name2 = 'Jim';
console.log(name2);

//If using single quote character, then statement will be interpreted weird because of apostrophe s in string
var statement = "This is Jim's string";
console.log(statement);

// Using single quotes allows for use of double quotes within string, and doublt quotes to define string value allows for use of single quote within value.
var statement2 = 'He said "This is awesome"';
console.log(statement2);

// If one has to use both single and double quotes, can use markers to make browser know that character is not to be regarded as code, but rather value in string.  Output: He said "This is Jim's string"
var statement3 = 'He said "This is Jim\'s string\"';
console.log(statement3);

// But what if there are also backslashes necessary to create a string?! To add an actual backslash, just use two of them together!
var path = "C:\\folder\\myfile.txt";
console.log(path);

// How do we create a string that multi-line or we need to indent?
var multiline = "This is line 1\nThis is line 2\nThis is line 3\n\tThis is a tabbed over line"
console.log(multiline);