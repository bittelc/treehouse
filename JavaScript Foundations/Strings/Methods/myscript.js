/* Methods for Strings 
=============================== */
var hello = "Hello"
var world = "World"
var helwo = hello + " " + world;

// ".length" is a suffix to determine the character length in the string. ".length" is called a method.
length = helwo.length;
console.log(helwo, ": ", length)

// To determine if a string of a specific character set is within the entire string, use ".indexOf()" method. Within the parenthesis should be the string that you'd like to test to see if its value is contained. 
var index = helwo.indexOf("worLD");
// Value of "index" variable is the beginning of where string sequence begins to match tested indexOf value.
console.log(index);
// Returns a -1 value  if tested not true
var index2 = helwo.indexOf("orLD");
console.log(index2);
if (helwo.indexOf("orLD") !== -1) {
	console.log("string of \"orLD\" exists within string at point: ", index2)
} else {
	console.log("string of \"orLD\" does not exist within string");
}

//"charAt()" method returns single character at element in string. 
console.log(helwo.charAt(2));




//This is how the console is cleared in Chrome!
console.clear();

/* Part II
=============================== */

//substr(start,length) takes two parameters. start index is the first character to start capturing. Length is the length of the string to capture.
var word=helwo.substr(6, 5); // The output here is word = "World"
console.log(word);

//"toLowerCase" converts the variable to lowercase letters. Because toLowerCase is a method, there must be parenthesis after it. Can apply to multiple variables with separation by comma. Same rules as "toUpperCase" method.
console.log("THISISTUESDAY",word.toLowerCase())
