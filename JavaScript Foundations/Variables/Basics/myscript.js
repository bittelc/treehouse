/* JavaScript Foundations: Variables */

/* "var" declares a variable*/
var color = "red";

/* "myDiv" declared variable accesses the element in the HTML document. Then the ".style" property is set. This is a better way than doing this:
document.getElementbyID('myDiv').style.background = "black";
document.getElementbyID('myDiv').style.color = "#ffffff"; */
var myDiv = document.getElementById('myDiv');
myDiv.style.background = "black";
myDiv.style.color = "#ffffff";