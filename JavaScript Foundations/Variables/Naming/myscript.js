/* JavaScript Foundations: Variables */

var color = "red";

var myDiv = document.getElementById('myDiv');
myDiv.style.background = "black";
myDiv.style.color = "#ffffff";

// Can start with any of: A-Z a-z _ $
// Continue with A-Z a-z 0-9 _ $
// Can't start with a number
/* Can't contain with any other special characters */

// Valid Variables
var car = "Toyota";
var Color = "blue";
var _myVariable = "something";
var $specialName = 1;
var a58389 = "What is this?";


/* Invalid Variable Names
var 3colors = "red green blue";
var winning% = 30;
var person-name = "Jim";  a "-" in Javascript is the same as a subtraction sign. Can't use
var @you = "are awesome!";
var continue = true;     "continue" in JavaScript is within the JS library, thus can't use. Can't use things like "if", "else", "true", "class" as variable names. */

