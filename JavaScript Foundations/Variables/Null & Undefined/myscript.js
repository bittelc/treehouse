/* JavaScript Foundations: Variables */

var color = "red";

var myDiv = document.getElementById('myDiv');
myDiv.style.background = "black";
myDiv.style.color = "#ffffff";

// Starts with A-Z a-z _ $
// Continue with A-Z a-z 0-9 _ $

// Valid Variables
var car = "Toyota";
var Color = "blue";
var _myVariable = "something";
var $specialName = 1;
var a58389 = "What is this?";


// Invalid Variable Names
// var 3colors = "red green blue";
// var winning% = 30;
// var person-name = "Jim";
// var @you = "are awesome!";

// Reserved words 
// var continue = true; */


/* --- Null & Undefined Lesson
=====================================*/
// Don't need to initialize the variable value. This simple declaration is fine. If you type into Chrome Developer Tools, "myVar" the result is "undefined". 
var myVar;

// undefined = true; 

// "console.log" prints to the console the type of information we want displayed. "typeof" asks if "myVar" is result of "undefined". The 3 equal signs tests for to see if this is true
console.log(typeof myVar === "undefined");
// Rather than using the string, this test applies to keyword "undefined". But keyword "undefined" may be overriden as a keyword, to a variable name. Like in the above code.
console.log(myVar === undefined);

// "null" is a keyword that means nothing. 
var x = null;

// Result of this code is printed to the console (inspect element in Chrome) is "If".
if(x == null){
  console.log("If");
} else {
  console.log("Else");
}

// This will also be true, because "null" and "undefined" are very similar. Oddly, the "==" double equal sign is like a "conceptually similar equalities?" question, whereas the "===" triple equal sign is like a "strictly exactly equal?".
if(myVar == null){
  console.log("If");
} else {
  console.log("Else");
}
// Typing "undefined == null", result will always be "true".
// However, typing "undefined === null" result will be "false"