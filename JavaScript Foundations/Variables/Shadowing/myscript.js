/* JavaScript Foundations: Variables */

var color = "red";

var myDiv = document.getElementById('myDiv');
myDiv.style.background = "black";
myDiv.style.color = "#ffffff";

// Starts with A-Z a-z _ $
// Continue with A-Z a-z 0-9 _ $

// Valid Variables
var car = "Toyota";
var Color = "blue";
var _myVariable = "something";
var $specialName = 1;
var a58389 = "What is this?";


// Invalid Variable Names
// var 3colors = "red green blue";
// var winning% = 30;
// var person-name = "Jim";
// var @you = "are awesome!";

// Reserved words 
// var continue = true;



// var myVar;
// 
// // undefined = true; 
// 
// console.log(typeof myVar === "undefined");
// console.log(myVar === undefined);
// 
// var x = null;
// 
// 
// if(myVar == null){
//   console.log("If");
// } else {
//   console.log("Else");
// }


// Scope

// var world = "World!";
// 
// function sayHello () {
//   var hello = "Hello ";
//   
//   function inner () {
//     var extra = " There is more!"
//     console.log(hello + world + extra);
//   }
//   
//   inner();
// }
// 
// sayHello();
// 
// console.log("world outside sayHello(): ", world);
// // console.log("hello outside sayHello(): ", hello);



/* --- Shadowing ----
======================= */
// Shadowing is using the same name for a variable

var myColor = "blue";
console.log("myColor before myFunc()", myColor);
var myNumber;

function myFunc () {
  var myColor = "yellow";
  myNumber = 42;
  //myOther is not a variable defined yet. But because we are in a function, JS by default assesses this as a variable
  myOther = "Created"
  // myColor inside the function is "yellow"
  console.log("myColor inside myFunc()", myColor);
}

myFunc();
// Variables within functions are only available within the function. Useful: within a function, can use any names for variables we want, because we won't override any declations in global scope.
console.log("myColor after myFunc()", myColor);
// Can't access "myNumber" because  within a function and can't access variables in function scope
console.log("myNumber after myFunc()", myNumber);
console.log("myOther after myFunc()", myOther);
/* But! if myFunc was instead of
var myColor  = "yellow"; 
it was
myColor = "yellow"
This is redefinig what myColor is equal to, rather than just creating a shadowed variable of the same name. This is actually revaluing the variable. In this case, the result of the console log outside of the function would be yellow.  This can be seen with the myNumber variable.  */
