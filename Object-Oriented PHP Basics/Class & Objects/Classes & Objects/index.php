<?php
class Product 
	//Product is not a keyword. Could have as well been "Fish" or "FISHY"
{
	// class definitions go here.
}

$p = new Product();
// "$p" object is now an instance of Product class. Must contain "$" preceding or else not an object.

// An object in OOP can be described as an individual instance of a Class by using the new operand.
?>