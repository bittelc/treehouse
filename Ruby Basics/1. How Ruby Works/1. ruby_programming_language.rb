=begin
This is a multiline comment
=end
#This is a single line comment
=begin
Interactive ruby can be activated in the console by typing in irb. This gives a Ruby prompt. From there, can then use console as ruby interpreter. Type things like puts "Hi!" to receive immediate output.
You can type "exit" in the Interactive Ruby to return to the command line.
Alternatively to Ruby Interactive in the console, you can create and use a Ruby code in your text editor and then run it in the terminal. In the terminal, access the ruby file created in text editor by typing:
"ruby file_path_and_name.rb 
=end

# "puts" is the command for outputting into the console
# commands do not need semicolons to execute in Ruby. Similar to VBA, the hard return to create a new lines indicates that the command has concluded.
puts "Hello World!"

# variable definitions in Ruby. Variables do not have to be independently declared and then assigned. Type declaration is done automatically.
name = "Cole"
puts name

# Puts and print are nearly the same command, however "puts" automatically ends the output with a line break whereas "print" continues on the same line
print "Print does not... "
puts "line break after it's called"
puts "Whereas put..."
puts "does not"

# To use a variable within a string output, use the "#{variable_Name}" syntax
this_Var = "to call this guy"
puts "I can still use syntax #{this_Var} even though she's a string"