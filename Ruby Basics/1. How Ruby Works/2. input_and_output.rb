#We've discussed already using the "puts" and "print" for outputting to the console

# Use the "gets" command to retrieve input from the user
# When using "gets" in the terminal, cursor in terminal will blink, meaning the terminal is awaiting your input
print "Please enter your name: "
name = gets
puts "Hello #{name}"