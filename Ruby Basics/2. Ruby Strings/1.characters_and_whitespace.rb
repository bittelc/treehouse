# New lines, tabs, and spaces can be represented with certain characters within something like a string.
# New line: \n
# Tab: \t
# Space: \s
print "This has line\nbreaks because of the characters\nin the string!\n\n"
puts "This has\sa tabbed in new line\n\tBecause it does\sand stuff!"