# variable definitions can be either within single or double quotes
name = 'Cole' # or name = "Cole"

# the same cannot be said for outputting strings! If you use single quotes around:
puts 'Hello #{name}' #output: Hello #{name}
puts "Hello #{name}" #output: Hello Cole
# This is called interpolation. Single quote does not do variable interpolation. Double quote does do variable interpolation

#Can also use "%q" to represent "'" (single quote) and "%Q" to represent """ (double quote)

#Can also have strings that span multiple lines
multi_Line = "
This is
a multiline
string !"
puts multi_Line

