=begin
Functions (methods) can be called by:
"variable_Name.method_Name"
Can also apply multiple methods within same line by linking them together
"string_Name.method_Name.method_Name.method_Name"...
=end

name = "Cole Bittel"
puts name.upcase #COLE BITTEL
puts name.downcase #cole bittel
puts name.reverse #lettiB eloC
puts name #Cole Bittel
revo = name.upcase
puts revo #COLE BITTEL

# Other methods
# Can use method ending in "!" to change variable assignment instead of having to write:
# var = var.upcase
# Can instead write:
# var.upcase!
