# Print out that we're adding two numbers together.
# And then return the sum of the two numbers.
def that_method(a, b)
  sums = a + b
  return "Adding #{a} and #{b} =  #{sums}"
end

string_thing = that_method(3, 4)
puts string_thing