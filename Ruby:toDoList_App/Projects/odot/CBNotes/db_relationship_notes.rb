=begin

The todo_list item has a foreign key called todo_list.id which specifies which specific item within the todo_list.

We need to add an association – a has_many / belongs_to association
	This is because one list has many list items

The "shoulda-matchers" gem allows us to match associations very easily without writing a ton of code
	Within Gemfile and the "test" group, add:
		"gem 'shoulda-matchers'""
	and then $ bundle

$ bin/rails generate model todo_item todo_list:references content:string
	can add a "-p" to end of command to see what command will actually do. -p stands for pretend.
	Generates a model, not a scaffold.

This entire part was very confusing. May need to review this entire set of videos.