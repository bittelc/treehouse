=begin

Old deleted notes (not testing notes)

$ vagrant up # launch virtual machine
$ vagrant ssh # login to VM

$ bin/rails server # launch local server
localhost:3000 for index page


$ (ctrl + C) # shutdown server
$ exit # logout of VM
$ vagrant halt # shutdown VM
=end






#======= Testing =============
=begin

$ bin/rspec /path_to_test_file/test_file.rb # to run file containing tests

For example:
$ bin/rspec spec/features/todo_lists/create_spec.rb # runs the tests in the "create_spec.rb" rile

Can run all tests in same command line
$ rake spec

=end
