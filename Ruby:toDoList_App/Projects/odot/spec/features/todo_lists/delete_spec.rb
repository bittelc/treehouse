require 'spec_helper'

describe "Deleting todo lists" do

	# Create dummy list item so that it can later be destroyed. Ensures that test will not fail due to there being no todo items in list and therefore no destroy button.
	let!(:todo_list) { TodoList.create(title: "Groceries", description: "Grocery list.") }
	it "is successful when clicking destoy link" do 
  		visit "/todo_lists"

  	within "#todo_list_#{todo_list.id}" do
  		click_link "Destroy"
  	end
  	expect(page).to_not have_content(todo_list.title)
  	expect(TodoList.count).to eq(0)
  end 
end