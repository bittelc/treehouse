/* jQuery intro
jQuery allows for using of CSS selectors to apply interactivity. It was developed by John Resig in 2006. It is, by nature, unobtrusive JavaScript - putting the user first and allows for good user experience regardless of browser or device. Obtrusive means getting in the way of the user. Unobtrusive means indiscriminatory of user.
api.jquery.com is the documentation for jQuery. Very good syntax and help for getting it right.
*/

//Hide Warning
// jQuery(".warning").hide(); //uses jQuery. Selects all elements in ".warning" class and hides them
//Show Warning Slowly
// jQuery(".warning").show("slow"); // now shows "warning" elements in a slow manner



/* combining jQuery is very easy using method chaining. If using the same selector, just concatenate them. Completely replaces above code. A shorthand version of writing "jQuery" before every line is using the "$" symbol. */
$(".warning").hide().show("slow");