/*
// Create function called "myCode" that hides and then shows slowly
var myCode = function() {
	$(".warning").hide().show("slow");
}

// Now need to actually run the function. The below code runs "myCode" whenever the entire DOM has been loaded in HTML.
$(Document).ready(myCode);
*/

// The above code can be entirely replaced by creating an anonymous function. The below does that 
$(document).ready(function() {
  $(".warning").hide().show("slow");
});