/* 4 P's of problem solving
Preparation - understand problem and develop solution
Plan out solution
Perform solution
Perfect - Tinker

In this project, develop spoiler revelear telling person that Darth Vader is Luke Skywalker's father. 
Prevent spoiler-phobes from seeing spoilers. Hide spoilers and reveal if they click a button */

// hide <span> because span contains all text. Need t oadd button within .spoiler.

$(".spoiler span").hide() // applies to all "span" elements within spoiler class

// applies a button to the end of the .spoiler class (which gives the button it's location and style
$(".spoiler").append("<button>Reveal Spoiler!</button>");

// selects all buttons on page (but only 1 button on page. Could also do (".spoiler button") to select all buttons within spoiler
/*
$("button").click(function() {
  $(".spoiler span").show();
  $(this).remove();
});
*/
$("button").hover(
  function() {
    $(".spoiler span").show("slow");
    $(this).detach();
},
  function() {
    $(".spoiler span").hide();
    $(".spoiler").append("<button>Reveal Spoiler!</button>");
  });