/* Problem: We want to make a dropdown menu for when the browser gets too small to display all the links. When we shrink the browser width (or device), not all the links are displayed (not responsive).
	Solution: Hide the text links and swap them out with a menu icon and navigate page from there */

/* Plan:
- Create a select and append-to menu

- Cycle over menu links
	- Create an option 
	- Option's value is the href of the link
	- Option's text is the text of the link
	- Append option to select
- Create button 
- Bind to click to button 
	Go to select's location 
- Modify CSS to hide links on small widths and show button and select
- Modify CSS to hide select button on larger widths and show links again.
- Deal with selected options depending on which page user is on */

var $select = $("<select></select>"); // Create select element to append to menu
//console.log(getElementById("selected").text())
//$select.text(getElementByClassName("selected").text());
$("#menu").append($select); // Append select element to menu
	
$("#menu a").each(function () { // .each iterates over a jQuery object, executing a function for each matched element
	var $anchor = $(this); // $anchor = the entire anchor element (including all atributes)
	var $option = $("<option></option>"); // create option for this anchor
	if($anchor.parent().hasClass("selected")) {
		$option.prop("selected", true);
	}
	$option.val($anchor.attr("href"));	// option value attribute = anchor href attribute
	$option.text($anchor.text()); // option text (<option>text</option) text = $anchor text
	$select.append($option); // append to select
}); // All links that are grandchildren of menu. This entire loop just parallels and copies data from href into an option dropdown box
/* Create button for going to page selected in dropdown box 
var $button = $("<button>Go</button>");
$("#menu").append($button);
$button.click(function() {
	window.location = $select.val(); // get selected elements value as opposed to 'set' as above
}); */
$select.change(function() {
	window.location = $select.val();
})



