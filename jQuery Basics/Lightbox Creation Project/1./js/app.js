/*
Problem: when we open our webpage (index.html) and we click on an image, we are taken to a site where we can no longer access anything else and have to use the browser's back button to return to an interactive site. 
Solution: Use jQuery to, instead of taking user to a new page, in the same page bring clicked image to forefront of windew, blow it up to larger size, and blacken out all other images on screen. Simply, create an overlay with a larger image. An overlay is called a "Lightbox".*/

/* Steps to create solution
1. Capture click event on a link to an image
  1.1 Show the overlay
  1.2 Update overlay with image linked in the link
  1.3 Get child's alt attribute and set caption to this text
2. Add overlay
  2.1 An image
  2.2 A caption
3. When overlay is clicked
	3.1 Hide the overlay
*/
var $overlay = $('<div id="overlay"></div>'); // variable to represent added overlay
var $image = $("<img>");
var $caption = $("<p></p>");
$overlay.append($image); // add the image element to the overlay
$overlay.append($caption); // add the caption element to the overlay (after (below) the image element)
$("body").append($overlay); // add the overlay element to the body of the index file.

$("#imageGallery a").click(
	function(event){ // this equals whichever link anchor is clicked
		event.preventDefault(); //This prevents the actual anchor to be opened (because the default action of click event on an anchor element is to take the browser to that event). This is because we don't want the user to go to another page, but instead want them to have a larger pop-up on the same page.
		var imageLocation = $(this).attr("href"); // return a string with the href location to its value for the element that is clicked on (using "this"). For example, $imageLocation = "images/thisImage/thatImage/image.png". It's a location!
		$image.attr("src", imageLocation); // Now locate the image source to the value of the href
		var captionText = $(this).children("img").attr("alt"); //because the alt attribut is an attribut of the image which  is a child of the anchor element, we can't simply access it through the anchor tag. We must navigate to the image element first.

		$caption.text(captionText);

		//console.log(this); //check to make sure the correct href is being called
		$overlay.show();
	}
); //

$overlay.click(
	function() {
		$overlay.hide(); // when overlay is clicked (overlay only opens when image is clicked), it then hides the image. Because the image is an element within the overlay, one can also click the lightbox modal (image) and the overlay will disappear.
	}
)