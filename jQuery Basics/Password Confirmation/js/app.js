// Preparation & problem
/*
Problem: We have form with little speech bubbles w/ "enter password longer than 8 characters" but it doesn't hide when we actually do enter an 8 character password. Same with confirm your password.

Solution: Hide and show hints at appropriate times

Plan:
1) Hide hints
- When event happens on password input
- When password is greater than 8 characters, hide. Else, show hints.

When event happens on confirmation input
-Find out if password and confirmation match
-Hide hint, if matched.
-Else, show hint */